;;; init-haskell.el --- Initialize haskell mode    -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Oleksandr Manenko

;;; Commentary:

;; 

;;; Code:

(require 'init-elpa)
(require-package 'haskell-mode)
(require-package 'intero)
;;(require-package 'hindent)

(add-hook 'haskell-mode-hook 'intero-mode)
;;(add-hook 'haskell-mode-hook #'hindent-mode)

;(require-package 'ghc)
;(require-package 'company-ghc)
;(setq ghc-debug t)
;(autoload 'ghc-init "ghc" nil t)
;(autoload 'ghc-debug "ghc" nil t)
;(add-hook 'haskell-mode-hook (lambda () (ghc-init)))
;(add-to-list 'company-backends 'company-ghc)

;; TODO: For some reason it doesn't work with local symbols


;; TODO: http://haskell.github.io/haskell-mode/manual/latest/Editing-Haskell-Code.html#Editing-Haskell-Code
;; TODO: (define-key haskell-mode-map (kbd "<f8>") 'haskell-navigate-imports)

(setq haskell-font-lock-symbols t)

(custom-set-variables
 '(haskell-stylish-on-save t))

(provide 'init-haskell)
;;; init-haskell.el ends here
