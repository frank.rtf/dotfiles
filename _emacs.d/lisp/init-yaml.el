;;; init-yaml.el --- Initialize yaml mode    -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Oleksandr Manenko

;;; Commentary:

;; 

;;; Code:

(require 'init-elpa)
(require-package 'yaml-mode)
(require 'yaml-mode)

(add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))

(provide 'init-yaml)
;;; init-yaml.el ends here
