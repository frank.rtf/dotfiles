;;; init-ui.el --- Init Emacs UI                     -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Oleksandr Manenko

;;; Commentary:

;;

;;; Code:

(require 'init-elpa)
(require-package 'atom-one-dark-theme)
(require-package 'golden-ratio)

(require 'golden-ratio)
(require 'atom-one-dark-theme)


(setq inhibit-startup-message t)
(menu-bar-mode -1)
(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode)
  (scroll-bar-mode -1))


;; TODO: Refactor the font selection logic by using the following alist
(setq manenko/fonts
      '(("Share Tech Mono"  . "-UKWN-Share Tech Mono-normal-normal-normal-*-19-*-*-*-d-0-iso10646-1")
        ("Monoid"           . "-*-Monoid-light-normal-semicondensed-*-16-*-*-*-m-0-iso10646-1")
        ("Inconsolata"      . "-PfEd-Inconsolata-normal-normal-normal-*-19-*-*-*-m-0-iso10646-1")
        ("Roboto Mono"      . "-pyrs-Roboto Mono-normal-normal-normal-*-21-*-*-*-*-0-iso10646-1")
        ("DejaVu Sans Mono" . "-PfEd-DejaVu Sans Mono-normal-normal-normal-*-21-*-*-*-m-0-iso10646-1")))


(cond

 ((find-font (font-spec :name "DejaVu Sans Mono"))
  (set-frame-font "-PfEd-DejaVu Sans Mono-normal-normal-normal-*-17-*-*-*-m-0-iso10646-1"))

 ((find-font (font-spec :name "SF Mono"))
  (set-frame-font "-*-SF Mono-normal-normal-normal-*-11-*-*-*-m-0-iso10646-1"))

 ((find-font (font-spec :name "Share Tech Mono"))
  (set-frame-font "-UKWN-Share Tech Mono-normal-normal-normal-*-19-*-*-*-d-0-iso10646-1"))

 ((find-font (font-spec :name "Monoid"))
  (set-frame-font "-*-Monoid-light-normal-semicondensed-*-16-*-*-*-m-0-iso10646-1"))

 ((find-font (font-spec :name "Inconsolata"))
  (set-frame-font "-PfEd-Inconsolata-normal-normal-normal-*-16-*-*-*-m-0-iso10646-1"))

 ((find-font (font-spec :name "Roboto Mono"))
  (set-frame-font "-pyrs-Roboto Mono-normal-normal-normal-*-16-*-*-*-*-0-iso10646-1")))


(setq
      x-select-enable-clipboard           t
      x-select-enable-primary             t
      save-interprogram-paste-before-kill t
      apropos-do-all                      t
      mouse-yank-at-point                 t)



(atom-one-dark-with-color-variables
  (custom-theme-set-faces
   'atom-one-dark
   `(highlight-doxygen-comment    ((t (:background "#24272e"))))
   `(highlight-doxygen-code-block ((t (:background "#202329"))))))


(if (display-graphic-p)
    (progn
      (load-theme 'atom-one-dark t)
      ;; (when (not (eq system-type 'darwin))
      ;; (load-theme 'atom-one-dark t))
      (toggle-frame-maximized))
  (progn
					;(load-theme 'manoj-dark)
    ))

(global-display-line-numbers-mode 1)
(column-number-mode 1)

(blink-cursor-mode 0)
(setq-default cursor-type 'bar)

;;(set-cursor-color "#cccccc")
(setq ring-bell-function 'ignore)

(golden-ratio-mode 1)


(provide 'init-ui)
;;; init-ui.el ends here
