;;; init-snippets.el --- YASnippet configuration     -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Oleksandr Manenko

;;; Commentary:

;;

;;; Code:

(require 'init-elpa)

(require-package 'yasnippet)
(require-package 'yasnippet-snippets)
(require-package 'yatemplate)

(require 'yasnippet)
(require 'yatemplate)
(require 'autoinsert)

(yas-global-mode  1)

(auto-insert-mode)
(setq auto-insert-query nil)

(yatemplate-fill-alist)

;; (defun manenko-find-git-repo (dir)
;;   (if (string= "/" dir)
;;       nil
;;     (if (file-exists-p (expand-file-name ".git/" dir))
;;         dir
;;       (manenko-find-git-repo (expand-file-name "../" dir)))))

;; (defun manenko-find-project-root ()
;;   (interactive)
;;   (if (ignore-errors (projectile-project-root))
;;       (projectile-project-root)
;;     (or (manenko-find-git-repo (buffer-file-name)) (file-name-directory (buffer-file-name)))))

(defun manenko-file-name-relative-to-project-root ()
  (interactive)
  (file-relative-name (buffer-file-name)
                      (projectile-project-root)))

(provide 'init-snippets)
;;; init-snippets.el ends here
