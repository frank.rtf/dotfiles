;;; init-cpp.el --- Initialize C++ mode    -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Oleksandr Manenko

;;; Commentary:

;; 

;;; Code:

(require 'init-elpa)
(require-package 'flycheck)
(require-package 'rtags)
(require-package 'cmake-ide)
(require-package 'flycheck-rtags)
(require-package 'company-rtags)
(require-package 'modern-cpp-font-lock)
(require-package 'flymake-cppcheck)
(require-package 'clang-format)
(require-package 'highlight-doxygen)

(require 'electric)
(require 'eldoc)
(require 'flycheck)
(require 'company-clang)
(require 'rtags)
(require 'company-rtags)
(require 'flycheck-rtags)
(require 'cmake-ide)
(require 'modern-cpp-font-lock)
(require 'flymake-cppcheck)
(require 'clang-format)
(require 'highlight-doxygen)

(add-hook 'c++-mode-common-hook (lambda ()
                                  (flycheck-select-checker 'rtags)
                                  (setq-local flycheck-highlighting-mode nil) ;; RTags creates more accurate overlays.
                                  (setq-local flycheck-check-syntax-automatically nil)))


(setq rtags-completions-enabled t)
(push 'company-rtags company-backends)
(setq rtags-autostart-diagnostics t)
(rtags-enable-standard-keybindings)

(cmake-ide-setup)
(electric-pair-mode 1)

(setq c-default-style "k&r"
      c-basic-offset 4)

(c-set-offset 'innamespace 0)

(add-hook 'c-mode-hook
          (lambda ()
            (setq indent-tabs-mode nil)
            (setq show-trailing-whitespace t)))

;; TODO: flymake-cppcheck
;; TODO: https://github.com/ludwigpacifici/modern-cpp-font-lock/blob/master/README.md
(add-hook 'c++-mode-hook #'modern-c++-font-lock-mode)
(add-hook 'c-mode-hook 'flymake-cppcheck-load)
(add-hook 'c++-mode-hook 'flymake-cppcheck-load)

(add-hook 'c-mode-hook 'highlight-doxygen-mode)
(add-hook 'c++-mode-hook 'highlight-doxygen-mode)

(global-set-key (kbd "C-c i") 'clang-format-region)
(global-set-key (kbd "C-c u") 'clang-format-buffer)

(provide 'init-cpp)
;;; init-cpp.el ends here
