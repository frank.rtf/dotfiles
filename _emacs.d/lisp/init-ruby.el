;;; init-ruby.el --- Initialize ruby mode    -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Oleksandr Manenko

;;; Commentary:

;; 

;;; Code:

(require 'init-elpa)
(require-package 'rvm)



(provide 'init-ruby)
;;; init-ruby.el ends here
