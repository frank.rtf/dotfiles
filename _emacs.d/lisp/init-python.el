;;; init-python.el --- Initialize python mode    -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Oleksandr Manenko

;;; Commentary:

;; 

;;; Code:

(require 'init-elpa)

(require-package 'pyenv-mode)
(require-package 'elpy)

(require 'elpy)

(elpy-enable)
(pyenv-mode)

(defun activate-pyenv-hook ()
  "Automatically activates pyenv version if .python-version file exists."
  (f-traverse-upwards
   (lambda (path)
     (let ((pyenv-version-path (f-expand ".python-version" path)))
       (if (f-exists? pyenv-version-path)
           (pyenv-mode-set (s-trim (f-read-text pyenv-version-path 'utf-8))))))))

;; TODO: This doesn't work with pymake. It seems it activates before this hook?
(add-hook 'find-file-hook 'activate-pyenv-hook)

(provide 'init-python)
;;; init-python.el ends here
