;;; init-git.el --- Use magit for git   -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Oleksandr Manenko

;;; Commentary:

;; 

;;; Code:

(require 'init-elpa)
(require-package 'magit)

(global-set-key (kbd "C-x g") 'magit-status)

(provide 'init-git)
;;; init-git.el ends here
