;;; init-toml.el --- Initialize TOML support         -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Oleksandr Manenko

;;; Commentary:

;; 

;;; Code:

(require 'init-elpa)
(require-package 'toml-mode)

(require 'toml-mode)

(provide 'init-toml)
;;; init-toml.el ends here

