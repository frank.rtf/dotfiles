;;; init.el  --- The Emacs Initialization File
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:


;(package-initialize)

(add-to-list 'load-path (expand-file-name "lisp"      user-emacs-directory))
(add-to-list 'load-path (expand-file-name "site-lisp" user-emacs-directory))

(require 'init-elpa)
(require 'init-exec-path)
(require 'init-ui)
(require 'init-editing)
(require 'init-navigation)
(require 'init-miscellaneous)
(require 'init-company-mode)

(require 'init-snippets)

;(require 'init-asciidoc)
(require 'init-clojure)
;(require 'init-cmake)
;(require 'init-cpp)
(require 'init-git)
;(require 'init-glsl)
;(require 'init-haskell)
(require 'init-lisp)
(require 'init-markdown)
;(require 'init-ocaml)
;(require 'init-python)
;(require 'init-ruby)
;(require 'init-rust)
;(require 'init-scala)
(require 'init-web-mode)
(require 'init-yaml)
;(require 'init-toml)
;(require 'enable-commands)

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))

(unless (file-exists-p custom-file)
  (write-region "" nil custom-file))

(load custom-file)

(provide 'init)
;;; init.el ends here

